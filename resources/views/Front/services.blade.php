@extends('Front.layouts.master')
@section('title', 'Services')
@section('content')
<!-- main section -->
<section class="main-section bg-cover d-flex align-items-center" style="background-image: url('/assets/images/cover2.png')">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 mb-4 order-2 order-md-1">
                <div class="intro">
                    <h1 class="heading">{{ __('main.title_servicespage')}}</h1>
                    <p>{{ __('main.description_servicespage')}}</p>
                </div>
            </div>
            <div class="col-md-6 mb-4 order-1 order-md-2">
                <div class="home-image">
                    <img src="/assets/images/services.png" alt="services-image">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end main section -->

<!-- services section -->
<section class="our-services text-center">
    <div class="container">
        <div class="row">
            <!-- service -->
            <div class="col-sm-6 col-md-4 m-b-30">
                <div class="service">
                    <img src="/assets/images/our-services/mobile.png" alt="service-image">
                    <h3 class="service-title">{{ __('main.mobile_apps_servicespage')}}</h3>
                </div>
            </div>
            <!-- end service -->

            <!-- service -->
            <div class="col-sm-6 col-md-4 m-b-30">
                <div class="service">
                    <img src="/assets/images/our-services/web.png" alt="service-image">
                    <h3 class="service-title">{{ __('main.websites_servicespage')}}</h3>
                </div> 
            </div>
            <!-- end service -->

            <!-- service -->
            <div class="col-sm-6 col-md-4 m-b-30">
                <div class="service">
                    <img src="/assets/images/our-services/graphic.png" alt="service-image">
                    <h3 class="service-title">{{ __('main.graphic_design_servicespage')}}</h3>
                </div>
            </div>
            <!-- end service -->

        </div>
    </div>
</section>
<!-- end services section -->

<!-- article section -->
<section class="article">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 m-b-30 article-cover">
                <div class="article-image">
                    <img src="/assets/images/our-services/one.jpg" alt="article-image">
                </div>
            </div>

            <div class="col-md-6 m-b-30 article-content">
                <div>
                    <h2 class="article-title">{{ __('main.mobile_apps_servicespage')}}</h2>
                    <p class="article-text">{!! __('main.mobile_apps_service_servicespage') !!}</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end article section -->

<!-- article section -->
<section class="article">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 m-b-30 article-cover">
                <div class="article-image">
                    <img src="/assets/images/our-services/two.jpg" alt="article-image">
                </div>
            </div>

            <div class="col-md-6 m-b-30 article-content">
                <div>
                    <h2 class="article-title">{{ __('main.websites_servicespage')}}</h2>
                    <p class="article-text">
                    {!! __('main.website_service_servicespage') !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end article section -->

<!-- article section -->
<section class="article">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 m-b-30 article-cover">
                <div class="article-image">
                    <img src="/assets/images/our-services/three.jpg" alt="article-image">
                </div>
            </div>

            <div class="col-md-6 m-b-30 article-content">
                <div>
                    <h2 class="article-title">{{ __('main.graphic_design_servicespage')}}</h2>
                    <p class="article-text">
                    {!! __('main.graphic_design_service_servicespage') !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end article section -->

<!-- contact-us section -->
<section class="contact-us bg-cover" style="background-image: url('/assets/images/contact-us-bg.png')">
<div class="container">
    @include('Front.partials.complaints')
</div>
</section>
<!-- end contact-us section -->
@endsection