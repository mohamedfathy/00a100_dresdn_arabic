@extends('Front.layouts.master')
@section('title', 'About Us')
@section('content')
<!-- main section -->
<section class="main-section bg-cover d-flex align-items-center" style="background-image: url('/assets/images/cover2.png')">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 mb-4 order-2 order-md-1">
                <div class="intro">
                    <h1 class="heading">{{ __('main.title_aboutpage')}}</h1>
                    <p>{{ __('main.description_aboutpage')}}</p>
                </div>
            </div>
            <div class="col-md-6 mb-4 order-1 order-md-2">
                <div class="home-image">
                    <img src="/assets/images/about-us/1.png" alt="about-us-image">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end main section -->

<!-- our story section -->
<section class="our-story">
    <div class="container">
        <div class="heading text-center">
            <h2 class="heading-text">{{ __('main.ourstory_aboutpage')}}</h2>
        </div>

        <div class="row align-items-center justify-content-center">
            <div class="col-md-10 col-lg-6 m-b-30">
                <div class="logo-image">
                    <img src="/assets/images/about-us/logo.png" alt="logo-image">
                </div>
            </div>

            <div class="col-md-10 col-lg-6 m-b-30">
                <div class="intro">
                    <p class="intro-text">
                    {{ __('main.ourstory_description_aboutpage')}}
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end our story section -->

<!-- banner section -->
<section class="banner bg-cover" style="background-image: url('/assets/images/about-us/2.png')"></section>
<!-- end banner section -->

<!-- contact-us section -->
<section class="contact-us bg-cover" style="background-image: url('/assets/images/contact-us-bg.png')">
<div class="container">
    @include('Front.partials.complaints')
</div>
</section>
<!-- end contact-us section -->

<!-- map section -->
<section class="map">
    @include('Front.partials.map')
</section>
<!--end map section -->


@include('Front.layouts.end')
@endsection