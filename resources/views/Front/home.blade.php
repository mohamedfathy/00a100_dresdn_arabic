@extends('Front.layouts.master')
@section('title', 'Home')
@section('content')
<!-- main section -->
<section class="main-section bg-cover d-flex align-items-center"
style="background-image: url('/assets/images/home-cover.png')">
<div class="container">
    <div class="row align-items-center">
    <div class="col-md-6 mb-4 order-2 order-md-1">
        <div class="intro">
        <h1 class="heading">{{ __('main.slogan_homepage')}}</h1>
        <p>{{ __('main.slogan_description_homepage')}}</p>
        <a href="{{ route('services', app()->getLocale() )}}" class="btn main-btn white-btn">{{ __('main.explore_homepage')}}<i
            class="fas fa-chevron-right mx-1"></i></a>
        </div>
    </div>
    <div class="col-md-6 mb-4 order-1 order-md-2">
        <div class="home-image">
        <img src="/assets/images/home-image.png" alt="home-image">
        </div>
    </div>
    </div>
    <div class="text-center">
    <button class="scroll-btn"><i class="fas fa-chevron-down"></i></button>
    </div>
</div>
</section>
<!-- end main section -->

<!-- services section -->
<section class="services">
<div class="container">
    <div class="heading text-center">
    <h2 class="heading-text">{{ __('main.services_homepage')}}</h2>
    </div>
    <div class="col-xl-10 p-0 m-auto">
    <div class="row justify-content-center">
        <!-- start card -->
            <div class="col-sm-8 col-md-6 col-lg-4 m-b-30">
        <div class="card service">
            <div class="bg-cover card-header" style="background-image: url('/assets/images/service-bg.png')"></div>
            <div class="card-body">
            <img src="/assets/images/services/mobile.png" alt="service-image" class="card-image">
            <h5 class="card-title">{{ __('main.mobile_apps_homepage') }}</h5>
            <p class="card-text">{{ __('main.mobile_apps_service_homepage') }}</p>
            <a href="{{ route('services', app()->getLocale() )}}" class="btn main-btn">{{ __('main.details_homepage') }}</a>
            </div>
        </div>
        </div>
        <!-- end start card -->

        <!-- start card -->
        <div class="col-sm-8 col-md-6 col-lg-4 m-b-30">
        <div class="card service">
            <div class="bg-cover card-header" style="background-image: url('/assets/images/service-bg.png')"></div>
            <div class="card-body">
            <img src="/assets/images/services/websites.png" alt="service-image" class="card-image">
            <h5 class="card-title">{{ __('main.websites_homepage') }}</h5>
            <p class="card-text">{{ __('main.website_service_homepage') }}</p>
            <a href="{{ route('services', app()->getLocale() )}}" class="btn main-btn">{{ __('main.details_homepage') }}</a>
            </div>
        </div>
        </div>
        <!-- end start card -->

        <!-- start card -->
        <div class="col-sm-8 col-md-6 col-lg-4 m-b-30">
        <div class="card service">
            <div class="bg-cover card-header" style="background-image: url('/assets/images/service-bg.png')"></div>
            <div class="card-body">
            <img src="/assets/images/services/graphics.png" alt="service-image" class="card-image">
            <h5 class="card-title">{{ __('main.graphic_design_homepage') }}</h5>
            <p class="card-text">{{ __('main.graphic_design_service_homepage') }}</p>
            <a href="{{ route('services', app()->getLocale() )}}" class="btn main-btn">{{ __('main.details_homepage') }}</a>
            </div>
        </div>
        </div>
        <!-- end start card -->
    </div>
    </div>
</div>
</section>
<!-- end services section -->


<!-- contact-us section -->
<section class="contact-us bg-cover" style="background-image: url('/assets/images/contact-us-bg.png')">
<div class="container">
    @include('Front.partials.complaints')
</div>
</section>
<!-- end contact-us section -->

<!-- map section -->
<section class="map">
    @include('Front.partials.map')
</section>
<!--end map section -->
@endsection