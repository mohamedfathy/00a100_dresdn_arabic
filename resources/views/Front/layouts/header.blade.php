<body>
  <!-- header -->
  <header class="header">
    <!-- main-navbar -->
    <nav class="navbar navbar-expand-lg navbar-light main-navbar">
      <div class="container">
        <a class="navbar-brand" href="/{{app()->getLocale() }}" style="background-image: url('/assets/images/logo.png')"></a>
        <div class="menu-icon navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent">
          <span></span>
          <span></span>
          <span></span>
        </div>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- nav links -->
          <div class="flex-grow-1"></div>
          <ul class="navbar-nav">
            <li class="nav-item {{ Request::is('/') ? 'active ' : '' }}">
                <a class="nav-link" href="{{ route('home', app()->getLocale() )}}">{{ __('main.home_page') }}</a>
            </li>
            <!-- <li class="nav-item {{ Request::is('portfolio') ? 'active ' : '' }}">
                <a class="nav-link" href="/portfolio">Portfolio</a>
            </li> -->
            <li class="nav-item {{ Request::is('services') ? 'active ' : '' }}">
                <a class="nav-link" href="{{ route('services', app()->getLocale() )}}">{{ __('main.our_services_page')}}</a>
            </li>
            <li class="nav-item {{ Request::is('about') ? 'active ' : '' }}">
                <a class="nav-link" href="{{ route('about', app()->getLocale() )}}">{{ __('main.about_us_page')}}</a>
            </li>
            <!-- <li class="nav-item {{ Request::is('team') ? 'active ' : '' }}">
                <a class="nav-link" href="{{ route('team', app()->getLocale() )}}">{{ __('main.our_team_page')}}</a>
            </li> -->
            <li class="nav-item {{ Request::is('contact') ? 'active ' : '' }}">
                <a class="nav-link" href="{{ route('contact', app()->getLocale() )}}">{{ __('main.contact_us_page')}}</a>
            </li>
            <?php 
            $output = "";
            if (!App::isLocale('en')) {
              
              $output .= "<li class=\"nav-item navagition-last-item\" >";
              $output .= "<a class=\"nav-link\" href=\"". route(Route::currentRouteName(), 'en') . "\">En</a>";
              $output .= "</li>";
              echo $output;
            } else {
              $output .= "<li class=\"nav-item\">";
              $output .= "<a class=\"nav-link\" href=\"". route(Route::currentRouteName(), 'ar') . "\">Ar</a>";
              $output .= "</li>";
              echo $output;
            }
            ?>
        </ul>
        <!-- end nav links -->

        </div>
      </div>
    </nav>
    <!-- end main-navbar -->
  </header>
  <!-- end header -->