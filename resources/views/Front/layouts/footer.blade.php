  <!-- footer -->
  <footer class="footer bg-cver" style="background-image: url('/assets/images/footer.png')">
    <div class="container">
      <div class="row justify-content-between">
        <div class="col-lg-9 order-lg-1 order-2">
          <div class="content">
            <div class="row justify-content-between">
              <div class="col-md-6">
                <div class="links">
                  <h5>{{ __('main.contact_us') }}</h5>
                  <ul class="list-unstyled">
                    <li><a href="#"><i class="fas fa-phone mx-1"></i> <span class="phone-numbers">+20 155 007 2987 - +20 106 667 4627</span></a></li>
                    <li><a href="#"><i class="fab fa-whatsapp mx-1"></i> <span class="phone-numbers">+20 155 007 2987 - +20 106 667 4627</span></a></li>
                    <li><a href="#"><i class="fas fa-envelope mx-1"></i> <span class="phone-numbers">sales@dresdn.com - career@dresdn.com</span></a></li>
                    <li><a href="#"><i class="fas fa-map-marker-alt mx-1"></i>{{ __('main.company_address') }}</a></li>
                  </ul>
                </div>
              </div>

              

              <div class="col-md-5">
                <div class="links">
                  <h5>{{ __('main.browse') }}</h5>
                  <ul class="list-unstyled">
                    <!-- <li><a href="/portfolio">Our works</a></li> -->
                    <li><a href="{{ route('services', app()->getLocale() )}}">{{ __('main.our_services_link') }}</a></li>
                    <li><a href="{{ route('about', app()->getLocale() )}}"> {{ __('main.about_us_link') }}</a></li>
                    <!-- <li><a href="/team">{{ __('main.our_team_link') }}</a></li> -->
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-3 order-lg-2 order-1">
          <div class="intro ">
            <a href="/{{app()->getLocale() }}"><img src="/assets/images/footer-logo.png" alt="logo-image" class="logo"></a>
            <ul class="list-unstyled d-flex align-items-center  social-icons">
              <li><a href="https://www.facebook.com/Dresdn-111131776996903"><i class="fab fa-facebook-f"></i></a></li>
              <li><a href="https://twitter.com/mosab34224225"><i class="fab fa-twitter"></i></a></li>
              <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="copyright text-center">
      <p>{{ __('main.start_copyright') }} © <?php echo date('Y'); ?> {{ __('main.company_copyright') }}, {{ __('main.end_copyright') }}.</p>
    </div>
  </footer>
  <!-- end footer -->