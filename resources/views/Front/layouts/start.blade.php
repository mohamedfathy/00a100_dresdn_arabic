<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Home</title>
  <?php 
    if (App::isLocale('ar')) {
      echo "<link rel=\"stylesheet\" href=\"/assets/css/vendors/bootstrap.min.css\">";
    } else {
      echo "<link rel=\"stylesheet\" href=\"/assets/css/vendors/bootstrap-rtl.min.css\">";
    }
  ?>
  <link rel="stylesheet" href="/assets/css/vendors/bootstrap.min.css">
  <link rel="stylesheet" href="/assets/css/vendors/slick.css">
  <link rel="stylesheet" href="/assets/css/main.css">
  <link rel="stylesheet" href="/assets/css/homepage.css">
  <link rel="stylesheet" href="/assets/css/portfolio.css">
  <link rel="stylesheet" href="/assets/css/our-services.css">
  <link rel="stylesheet" href="/assets/css/about-us.css">
  <link rel="stylesheet" href="/assets/css/our-team.css">
  <link rel="stylesheet" href="/assets/css/contact-us.css">
  <link rel="stylesheet" href="/assets/css/arabic.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
    integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Tajawal&display=swap" rel="stylesheet">

  <style>
  <?php 
    if (App::isLocale('ar')) {
      echo "body {font-family: 'Tajawal', sans-serif;}";
      echo "body {text-align: right;direction: rtl;}";
    } else {
      echo "body {font-family: 'Roboto', '', sans-serif;}";
    }
  ?>
  </style>
</head>
