<?php

return [
  // views/Front/layouts/header.blade.php'
    'home_page' => 'Home',
    'our_services_page' => 'Our Services',
    'about_us_page' => 'About Us',
    'our_team_page' => 'Our Team',
    'contact_us_page' => 'Contact Us',

  // views/Front/layouts/footer.blade.php'
    'contact_us' => 'Contact Us',
    'company_address' => '12 Mourad Bek, Almazah, Helioplis, Cairo',
    'browse' => 'Browse',
    'our_services_link' => 'Our Service',
    'about_us_link' => 'About Us',
    'our_team_link' => 'Our Team',
    'start_copyright' => 'Copyright',
    'company_copyright' => 'Dresdn Company',
    'end_copyright' => 'All rights reserved',

    
  // views/Front/partials/complaints.blade.php'
    'let_us_talk' => 'LET\'S TALK',
    'complaint_name' => 'Name',
    'complaint_mobile_number' => 'Mobile Number',
    'complaint_mobile_message' => 'Your Message',
    'complaint_submit' => 'Submit',

  // views/Front/home.blade.php'
    'slogan_homepage' => 'Perfect Is Possible',
    'slogan_description_homepage' => 'We always challenge ourselves to provide a top quality in our applications.',
    'explore_homepage' => ' Explore our services',
    'services_homepage' => 'SERVICES',
    'mobile_apps_homepage' => ' Mobile Apps',
    'mobile_apps_service_homepage' => ' Bring your application to reality. Our company offers creative and innovative expertise on Mobile Apps across all mobile platforms. ',
    'details_homepage' => 'Details',
    'websites_homepage' => 'Websites',
    'website_service_homepage' => 'Build your own customized website. Our company offers simple user experience and rubust websites that users will love.',
    'graphic_design_homepage' => 'Graphic Design',
    'graphic_design_service_homepage' => 'Show your character to the World. Our company offers Micro services Logo, Motion Graphic and etc...',

  // views/Front/services.blade.php'
    'title_servicespage' => 'OUR SERVICES',
    'description_servicespage' => 'Our company provide a range of services such as Mobile Apps, Web Apps and more.',
    'mobile_apps_servicespage' => 'Mobile Apps',
    'websites_servicespage' => 'Web Apps',
    'graphic_design_servicespage' => 'Graphic Design',

    'mobile_apps_service_servicespage' => 'Mobile applications are being increasingly used by people in the region and more and more businesses are 
    realizing the need for mobile apps to cater to their customers.
    <br /> Mobile applications are making everyday tasks simpler.',
    'website_service_servicespage' => 'The custom web applications we develop easily become an integral part of existing business systems. 
    <br /> Leveraging our cross-industrial experience. we tailor solutions to our customers individual needs.',
    'graphic_design_service_servicespage' => 'With the many distractions that the modern world has to offer, it can be difficult to achieve a high level of productivity that gets you rewarding work. 
                  <br />Our company provide a great state of art which our customer will love.',

  // views/Front/about.blade.php'
    'title_aboutpage' => 'ABOUT US',
    'description_aboutpage' => 'Dresdn is a startup company providing solutions in Web and Mobile applications.',
    'ourstory_aboutpage' => ' OUR STORY',
    'ourstory_description_aboutpage' => 'Dresdn is a new Software House that provides expertise in Mobile Applications solutions for businesses worldwide, It also provide Web Applications programming, graphic design and search engine optimization techniques to improve its clients\' positioning in search engines.',

  // views/Front/contact.blade.php'
    'title_contactpage' => 'CONTACT US',
    'description_contactpage' => 'We’re here to help and answer any question you might have. We look forward to hearing from you.',
    'keepintouch_contactpage' => 'we will be in touch'
];