<?php

return [
  // views/Front/layouts/header.blade.php'
    'home_page' => 'الرئيسية',
    'our_services_page' => 'خدماتنا',
    'about_us_page' => 'عن الموقع',
    'our_team_page' => 'فريق العمل',
    'contact_us_page' => 'اتصل بنا',


  // views/Front/layouts/footer.blade.php'
    'contact_us' => 'اتصل بنا',
    'company_address' => '12 شارع مراد بيك - الماظة - مصر الجديدة - القاهرة',
    'browse' => 'تصفح',
    'our_services_link' => 'خدماتنا',
    'about_us_link' => 'عن الموقع',
    'our_team_link' => 'فريق العمل',
    'start_copyright' => 'حقوق النشر',
    'company_copyright' => 'شركة ديرسدن',
    'end_copyright' => 'جميع الحقوق محفوظة',

  // views/Front/partials/complaints.blade.php'
    'let_us_talk' => 'دعنا نتحدث',
    'complaint_name' => 'الاسم',
    'complaint_mobile_number' => 'رقم الهاتف',
    'complaint_mobile_message' => 'الرسالة',
    'complaint_submit' => 'ارسل',

  // views/Front/home.blade.php'
    'slogan_homepage' => 'جيد ليست كافية',
    'slogan_description_homepage' => 'دائما ما نتحدى انفسنا فى تقديم افضل جودة فى تطبيقتنا.',
    'explore_homepage' => 'استكشف خدماتنا',
    'services_homepage' => 'الخدمات',
    'mobile_apps_homepage' => 'تطبيقات الموبايل',
    'mobile_apps_service_homepage' => 'اصنع التطبيق الخاص بك. شركتنا تقدم خبرات مبتكرة و مبدعة فى مجال تطبيقات الموبايل.لكل المنصات المختلفة.',
    'details_homepage' => 'تفاصيل اكثر',
    'websites_homepage' => 'مواقع الويب',
    'website_service_homepage' => 'اصنع موقع الويب الخاص بك. شركتنا تقدم تجربة مستحدم بسيطة و سريعة .سوف يحبها المستخدم بالتاكيد.',
    'graphic_design_homepage' => 'تصميم الرسوم',
    'graphic_design_service_homepage' => 'عبر عن شخصيتك للعالم . شركتنا تقدم خدمات مصغرة مثل تصميم الشعار و الرسوم المتحركة الخ...',

  // views/Front/services.blade.php'
    'title_servicespage' => 'خدماتنا',
    'description_servicespage' => 'تقدم شركتنا مجموعة متنوعة من الخدمات مثل خدمات الويب و الموبايل ابلكيشين و تصميم الرسوم.',
    'mobile_apps_servicespage' => 'تطبيقات الموبايل',
    'websites_servicespage' => 'مواقع الويب',
    'graphic_design_servicespage' => 'تصميم الرسوم',

    'mobile_apps_service_servicespage' => 'يتم استخدام تطبيقات الهاتف بشكل متزايد فى المنطقة ويدرك رواد الاعمال الحاجة الى تطبيقات المحمول لتلبية احتياجات عملائهم. <br />
    تجعل تطبيقات الهاتف مهامنا اليومية سهلة  ',

    'website_service_servicespage' => 'اصبحت تطبيقات الويب المخصصة التى نطورها جزء لا يتجزا من انظمة العمل اليومية لدى الشركات. نحن نلبى كل احتياجات عملائنا.',

    'graphic_design_service_servicespage' => 'مع العديد من عوامل التشتيت التي يقدمها العالم الحديث ، قد يكون من الصعب تحقيق مستوى عالٍ من الإنتاجية يجعلك مجزيًا في العمل.
                       <br /> توفر شركتنا حالة فنية رائعة سيحبها عملاؤنا.',

  // views/Front/about.blade.php'
    'title_aboutpage' => 'عن الموقع',
    'description_aboutpage' => 'ديرسدن شركة ناشئة توفر خدمات تصميم و برمجة تطبيقات الموبايل و مواقع الويب.',
    'ourstory_aboutpage' => 'قصتنا',
    'ourstory_description_aboutpage' => 'ديرسدن هي شركة برمجيات جديدة توفر الخبرة في مجال حلول تطبيقات الأجهزة المحمولة للشركات في جميع أنحاء العالم ، كما توفر برمجة تطبيقات الويب ، وتصميم الرسوم البيانية وتقنيات تحسين محركات البحث لتحسين مواقع عملائها في محركات البحث.',
  
  // views/Front/contact.blade.php'
    'title_contactpage' => 'تواصل معنا',
    'description_contactpage' => 'نحن هنا للمساعدة والإجابة على أي سؤال قد يكون لديك. نحن نتطلع الى الاستماع منك.',
    'keepintouch_contactpage' => 'يسعدنا تواصلك معنا'
];