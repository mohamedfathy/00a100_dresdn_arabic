<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/en');
Route::group(['prefix' => '{language}'], function () {
  Route::get('/', 'Front\FrontController@home')->name('home');
  Route::post('/complaints', 'Front\FrontController@complaints')->name('complaints');
  Route::get('/portfolio', 'Front\FrontController@portfolio')->name('portfolio');
  Route::get('/services', 'Front\FrontController@services')->name('services');
  Route::get('/about', 'Front\FrontController@about')->name('about');
  Route::get('/team', 'Front\FrontController@team')->name('team');
  Route::get('/contact', 'Front\FrontController@contact')->name('contact');
});
